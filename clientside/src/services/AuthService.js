import config from 'config'
import axios from 'axios'
export const AuthService = {
    login,
    logout,
    register,
    userSession
};

function login(email, password) {
    return axios.post('http://localhost:5000/api/auth/login', 
        JSON.stringify({ email, password }), // the data to post
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data._id
    })
    .catch((err) => {
        console.log(err);
    })
}

function userSession(){
    return axios.get('http://localhost:5000/api/auth/')
        .then((res) => {
            console.log("RESPONSE RECEIVED: ", res);
            return res.data._id
        })
        .catch((err) => {
            console.log("AXIOS ERROR: ", err);
        })
}
function logout() {
    // remove user from local storage to log user out
}

function register() {
    console.log('trigger register')
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text)
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout()
                location.reload(true)
            }

            const error = (data && data.message) || response.statusText
            return Promise.reject(error)
        }

        return data
    });
}