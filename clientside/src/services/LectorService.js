import config from 'config'
import axios from 'axios'
export const LectorService = {
    add,
    update,
    getAll,
    getLectorDetails
};

function add(data) {
    return axios.post(config.apiURL+'/lectors/add', 
        data, // the data to post
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data.msg
    })
    .catch((err) => {
        return err.response.data
    })
}
function update(data) {
    const id = data._id
    return axios.post(config.apiURL+'/lectors/update/'+id, 
        data, // the data to post
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data.msg
    })
    .catch((err) => {
        return err.response.data
    })
}
function getAll() {
    return axios.get(config.apiURL+'/lectors/',
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err.response.data.msg
    }) 
}

function getLectorDetails(id) {
    return axios.get(config.apiURL+'/lectors/edit/'+id,
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err.response.data.msg
    }) 
}