import config from 'config'
import axios from 'axios'

export const TagService = {
    add,
    getAll
}

function add(data) { 
    return axios.post(config.apiURL+'/tags/add',
        data,
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data.msg
    })
    .catch((err) => {
        return err.response.data
    })
}

function getAll() {
    return axios.get(config.apiURL+'/tags/',
        { headers: { 'Content-Type': 'application/json'}
    })
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err.response.data.msg
    }) 
}