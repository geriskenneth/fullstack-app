import config from 'config'
import axios from 'axios'
export const CourseService = {
    add,
    update,
    getAll,
    getCourseDetails
};

function add(data) {
    return axios.post(config.apiURL+'/courses/add', 
        data, // the data to post
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data.msg
    })
    .catch((err) => {
        return err.response.data
    })
}
function update(data) {
    const id = data._id
    return axios.post(config.apiURL+'/courses/update/'+id, 
        data, // the data to post
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data.msg
    })
    .catch((err) => {
        return err.response.data
    })
}
function getAll() {
    return axios.get(config.apiURL+'/courses/',
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err.response.data.msg
    }) 
}

function getCourseDetails(id) {
    return axios.get(config.apiURL+'/courses/edit/'+id,
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err.response.data.msg
    }) 
}