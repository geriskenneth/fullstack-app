import config from 'config'
import axios from 'axios'

export const CategoryService = {
    add,
    getAll
}

function add(data) { 
    return axios.post(config.apiURL+'/categories/add',
        data,
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data.msg
    })
    .catch((err) => {
        return err.response.data
    })
}

function getAll() {
    return axios.get(config.apiURL+'/categories/',
        { headers: { 'Content-Type': 'application/json'}
    })
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err.response.data.msg
    }) 
}