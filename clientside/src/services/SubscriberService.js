import config from 'config'
import axios from 'axios'
export const SubscriberService = {
    add,
    update,
    getAll,
    getSubscriberDetails
}

function add(data) {
    return axios.post(config.apiURL+'/subscribers/add', 
        data, // the data to post
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data.msg
    })
    .catch((err) => {
        return err.response.data
    })
}
function update(data) {
    const id = data._id
    return axios.post(config.apiURL+'/subscribers/update/'+id, 
        data, // the data to post
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data.msg
    })
    .catch((err) => {
        return err.response.data
    })
}
function getAll() {
    return axios.get(config.apiURL+'/subscribers/',
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err.response.data.msg
    }) 
}

function getSubscriberDetails(id) {
    return axios.get(config.apiURL+'/subscribers/edit/'+id,
        { headers: {'Content-type': 'application/json'}
    })
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err.response.data.msg
    }) 
}