import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/dist/vuetify.min.css'
Vue.use(Vuetify)

export default new Vuetify({
  theme: {
      light: true,
    themes: {
      light: {
        primary: "#00838F",
        secondary: '#A7BF2E',
        accent: '#F95537',
        error: '#b71c1c',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
      }
    }
  }
})
