import { CategoryService } from '../../services/CategoryService'
export const categories = {
    namespaced: true,
    state: {
        CATEGORIES: []
    },
    mutations: {
        SET_CATEGORIE(state, data) {
            state.CATEGORIES.push(data)
        },
        GET_CATEGORIES(state, data) {
            state.CATEGORIES = data
        }
    },
    actions: {
        SET_CATEGORY({commit}, data) {
            CategoryService.add(data)
            .then(
                category => {
                    commit('SET_CATEGORY', data)
                },
                error => {
                    console.log(error)
                }
            )
        },
        GET_CATEGORIES({commit}, data) {
            CategoryService.getAll()
                .then(
                    categories => {
                        commit('GET_CATEGORIES', categories)
                    },
                    error => {
                        console.log(error)
                    }
                )
        }
    },
    getters: {
        GET_CATEGORIE_COUNT(state) {
            return state.CATEGORIES.length
        }
    }
}