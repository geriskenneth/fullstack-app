import { TagService } from '../../services/TagService'
export const tags = {
    namespaced: true,
    state: {
        TAGS: []
    },
    mutations: {
        SET_TAG(state, data) {
            state.TAGS.push(data)
        },
        GET_TAGS(state, data) {
            state.TAGS = data
        }
    },
    actions: {
        SET_TAG({commit}, data) {
            TagService.add(data)
            .then(
                tag => {
                    commit('SET_TAG', data)
                },
                error => {
                    console.log(error)
                }
            )
        },
        GET_TAGS({commit}, data) {
            TagService.getAll()
                .then(
                    tags => {
                        commit('GET_TAGS', tags)
                    },
                    error => {
                        console.log(error)
                    }
                )
            
        }
    },
    getters: {
        GET_TAG_COUNT(state) {
            return state.TAGS.length
        }
    }
}