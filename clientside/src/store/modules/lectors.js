import { LectorService } from '../../services/LectorService'

export const lectors = {
        namespaced: true,
        state: {
            LECTORS: [],
            LECTOR: {},
            ERROR: ''
        },
        mutations: {
            SET_LECTOR(state, data) {
                state.LECTORS.push(data)
            },
            GET_LECTORS(state, data){
                state.LECTORS = data
            },
            GET_LECTORDETAILS(state, data) { 
                state.LECTOR = data
            },
            UPDATE_LECTOR(state, data) {
                state.LECTOR = data
            }
        },
        actions : {
            SET_LECTOR({commit}, data) {
                LectorService.add(data)
                    .then(
                        lectors => {
                            commit('SET_LECTOR', data)
                        },
                        error => {
                            console.log(error)
                        }
                    )
            },
            GET_LECTORS({commit}, data) {
                LectorService.getAll()
                    .then(
                        lectors => {
                            commit('GET_LECTORS', lectors)
                        },
                        error => {
                            console.log(error)
                        }
                    )
            },
            GET_LECTORDETAILS({commit}, id) {
                LectorService.getLectorDetails(id)
                    .then(
                        lector => {
                            commit('GET_LECTORDETAILS', lector)
                        },
                        error => {
                            console.log(error)
                        }
                    )
            },
            UPDATE_LECTOR({commit}, id) {
                LectorService.update(id)
                    .then(
                        lector => {
                            commit('UPDATE_LECTOR', lector)
                        },
                        error => {
                            console.log(error)
                        }
                    )
            }
        },
        getters: {
            ALL_LECTORS(state) {
                return state.LECTORS.length
            },
            ACTIVE_LECTORS(state) {
                return state.LECTORS.filter(lector => lector.active == true).length
            },
            NONACTIVE_LECTORS(state) {
                return state.LECTORS.filter(lector => lector.active == false).length
            }
        }
}