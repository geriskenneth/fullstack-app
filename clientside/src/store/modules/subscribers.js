import { SubscriberService } from '../../services/SubscriberService'

export const subscribers = {
        namespaced: true,
        state: {
            SUBSCRIBERS: [],
            SUBSCRIBER: {},
            ERROR: ''
        },
        mutations: {
            SET_SUBSCRIBER(state, data) {
                state.SUBSCRIBER.push(data)
            },
            GET_SUBSCRIBERS(state, data){
                state.SUBSCRIBERS = data
            },
            GET_SUBSCRIBERDETAILS(state, data) { 
                state.SUBSCRIBER = data
            },
            UPDATE_SUBSCRIBER(state, data) {
                state.SUBSCRIBER = data
            }
        },
        actions : {
            SET_SUBSCRIBER({commit}, data) {
                SubscriberService.add(data)
                    .then(
                        subscriber => {
                            commit('SET_SUBSCRIBER', subscriber)
                        },
                        error => {
                            console.log(error)
                        }
                    )
            },
            GET_SUBSCRIBERS({commit}, data) {
                SubscriberService.getAll()
                    .then(
                        subscribers => {
                            commit('GET_SUBSCRIBERS', subscribers)
                        },
                        error => {
                            console.log(error)
                        }
                    )
            },
            GET_SUBSCRIBERDETAILS({commit}, id) {
                SubscriberService.getSubscriberDetails(id)
                    .then(
                        subscriber => {
                            commit('GET_SUBSCRIBERDETAILS', subscriber)
                        },
                        error => {
                            console.log(error)
                        }
                    )
            },
            UPDATE_SUBSCRIBER({commit}, id) {
                SubscriberService.update(id)
                    .then(
                        subscriber => {
                            commit('UPDATE_SUBSCRIBER', subscriber)
                        },
                        error => {
                            console.log(error)
                        }
                    )
            }
        },
        getters: {
            ALL_SUBSCRIBERS(state) {
                return state.SUBSCRIBERS.length
            },
            ACTIVE_SUBSCRIBERS(state) {
                return state.SUBSCRIBERS.filter(subscriber => subscriber.active == true).length
            },
            NONACTIVE_SUBSCRIBERS(state) {
                return state.SUBSCRIBERS.filter(subscriber => subscriber.active == false).length
            }
        }
}