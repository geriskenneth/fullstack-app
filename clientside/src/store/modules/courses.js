import { CourseService } from '../../services/CourseService'

export const courses = {
    namespaced: true,
    state: {
        COURSES: [],
        COURSEDETAILS: {},
        ERROR: ''
    },
    mutations: {
        SET_COURSE(state, data) {
            state.COURSES.push(data)
        },
        GET_COURSES(state, data) {
            state.COURSES = data
        },
        GET_COURSEDETAILS(state, data) {
            state.COURSEDETAILS = data
        }
    },
    actions: {
        SET_COURSE({commit}, data) {
            CourseService.add(data)
            .then(
                course => {
                    commit('SET_COURSE', data)
                },
                error => {
                    console.log(error)
                }
            )
        },
        GET_COURSES({commit}, data) {
            CourseService.getAll()
            .then(
                courses => {
                    commit('GET_COURSES', courses)
                },
                error => {
                    console.log(error)
                }
            )            
        }

    },
    getters: {
        ALL_COURSES(state) {
            return state.COURSES.length
        },
        ACTIVE_COURSES(state) {
            return state.COURSES.filter(course => course.active == true).length
        },
        NONACTIVE_COURSES(state) {
            return state.COURSES.filter(course => course.active == false).length
        }
    }
}