import { AuthService } from '../../services/AuthService'
import router from '../../router'

export const auth = {
    namespaced: true,
    state: {
        LOGGED_IN: false,
        USER: null,
        STATUS: {}
    },
    actions: {
        login({commit}, data) {
            const { email, password } = data
            
            AuthService.login(email, password)
                .then(
                    user => {
                        commit('loginSuccess', user)
                        router.push('/about')
                    },
                    error => {
                        commit('loginFailure', error)
                    }
                )
        },
        logout({ commit }) {
            AuthService.logout()
            commit('logout')
        },
        usersession({ commit }){
            AuthService.userSession()
        }
    },
    mutations: {
        loginSuccess(state, user) {
            state.LOGGED_IN = true
            state.USER = user
        },
        loginFailure(state) {
            state.STATUS = {}
            state.USER = null
        },
        logout(state) {
            state.STATUS = {}
            state.USER = null
        }
    }
}