import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import { auth } from './modules/auth'
import { courses } from './modules/courses'
import { lectors } from './modules/lectors'
import { categories } from './modules/categories'
import { tags } from './modules/tags'
import { subscribers } from './modules/subscribers'

export default new Vuex.Store({
  state: {
    loginform: false,
    layout: 'user-layout'
  },
  mutations: {
    setLoginForm (state, payload) {
      state.loginform = payload
    },
    SET_LAYOUT (state, payload) {
      state.layout = payload
    }
  },
  actions: {
  },
  getters: {
    layout (state) {
      return state.layout
    }
  },
  modules: {
    auth,
    lectors,
    courses,
    categories,
    tags,
    subscribers
  }
})
