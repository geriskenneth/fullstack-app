import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
const routes = [

  //User - Profile
  { path: '/user/',name: 'user',component: () => import('@/views/User/Index'),
    children: [
      { path: 'edit-avatar', name: 'edit-avatar', component: () => import('@/views/User/EditAvatar') },
      { path: 'edit-profile', name: 'edit-profile', component: () => import('@/views/User/EditProfile') },
      { path: 'edit-account', name: 'edit-account', component: () => import('@/views/User/EditAccount') }
    ]
  },



  //Dashboard - Home
  { path: '/admin/', name: 'admin', component: () => import('@/views/Admin'), 
    meta: {
        layout: 'admin-layout'
    }
  },

  //Dashboard - Lectors
  { path: '/admin/lectors/', name: 'lectors', component: () => import('@/views/Admin/Lectors'), meta: { layout: 'admin-layout' }, 
    children: [
      { path: 'overview', name: 'overview-lectors', component: () => import('@/views/Admin/Lectors/Overview'), 
        meta: {
          layout: 'admin-layout',      
          breadcrumb:[
            {text: 'Lectors', disabled: true}
          ]
        } 
      },
      { path: 'add', name: 'add-lectors', component: () => import('@/views/Admin/Lectors/Add'), 
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Lectors', to: '/admin/lectors/overview', disabled: false},
            {text: 'New', disabled: true}
          ]
        } 
      },
      { path: 'edit/:id', name: 'edit-lectors', component: () => import('@/views/Admin/Lectors/Edit'),
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Lectors', to: '/admin/lectors/overview', disabled: false},
            {text: 'Edit', disabled: true}
          ]
        } 
      },
    ]
  },

  //Dashboard - Courses
  { path: '/admin/courses/', name: 'admin-courses', component: () => import('@/views/Admin/Courses'), meta: {layout: 'admin-layout'}, 
    children: [
      { path: 'overview', name: 'overview-courses', component: () => import('@/views/Admin/Courses/Overview'),
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Courses', disabled: true},
          ]
        }     
      },
      { path: 'add', name: 'add-courses', component: () => import('@/views/Admin/Courses/AddCourse'),
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Courses', to: '/admin/courses/overview', disabled: false},
            {text: 'New Course', disabled: true}
          ]
        } 
      },
      { path: 'edit/:id', name: 'edit-courses', component: () => import('@/views/Admin/Courses/EditCourse'), 
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Courses', to: '/admin/courses/overview', disabled: false},
            {text: 'Edit Course', disabled: true}
          ]
        } 
      },
      
      { path: 'categories', name: 'admin-categories', component: () => import('@/views/Admin/Courses/Categories'),
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Courses', to: '/admin/courses/overview', disabled: false},
            {text: 'Categories', disabled: true}
          ]
        } 
      },
      { path: 'edit-category/:id', name: 'edit-category', component: () => import('@/views/Admin/Courses/EditCategory'),
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Courses', to: '/admin/courses/overview', disabled: false},
            {text: 'Edit Category', disabled: true}
          ]
        } 
      },

      { path: 'tags', name: 'tags', component: () => import('@/views/Admin/Courses/Tags'),
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Courses', to: '/admin/courses/overview', disabled: false},
            {text: 'Tags', disabled: true}
          ]
        }     
      },
      { path: 'edit-tag/:id', name: 'edit-tag', component: () => import('@/views/Admin/Courses/EditTag'),
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Courses', to: '/admin/courses/overview', disabled: false},
            {text: 'Edit Tag', disabled: true}
          ]
        } 
      },
    ]
  },

  //Dashboard - Subscriptions
  { path: '/admin/subscribers/', name: 'subscriptions', component: () => import('@/views/Admin/Subscriptions'), meta: {layout: 'admin-layout'}, 
    children: [
      { path: 'overview', name: 'overview-subcriptions', component: () => import('@/views/Admin/Subscriptions/Overview'), meta: {layout: 'admin-layout'} },
      { path: 'details/:id', name: 'details-subscriber', component: () => import('@/views/Admin/Subscriptions/Details/')}
    ]
  },

  //Dashboard - Inbox
  { path: '/admin/inbox/', name: 'inbox', component: () => import('@/views/Admin/Inbox'), meta: {layout: 'admin-layout'}, 
    children: [
      { path: 'overview', name: 'overview-inbox', component: () => import('@/views/Admin/Inbox/Overview'), 
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Inbox', to: '/admin/inbox/overview', disabled: true},
          ]
        } 
      },
      { path: 'compose', name: 'new-inbox', component: () => import('@/views/Admin/Inbox/New'), 
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Inbox', to: '/admin/inbox/overview', disabled: false},
            {text: 'New Message', disabled: true}
          ]
        } 
      },
      { path: 'details/:id', name: 'details-inbox', component: () => import('@/views/Admin/Inbox/Details'), 
        meta: {
          layout: 'admin-layout',
          breadcrumb:[
            {text: 'Inbox', to: '/admin/inbox/overview', disabled: false},
            {text: 'Details', disabled: true}
          ]
        } 
      }
    ]
  },

  //User (front-end)
  { path: '/', name: 'home', component: () => import('@/views/User/Index') },
  { path: '/courses/', name: 'courses', component: () => import('@/views/User/Courses') },
  { path: '/courses/:id', name: 'courses-id', component: () => import('@/views/User/Courses/Details') },
  { path: '/categories/', name: 'categories', component: () => import('@/views/User/Categories') },
  { path: '/category/:id', name: 'category-id', component: () => import('@/views/User/Categories/Overview') },


]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
