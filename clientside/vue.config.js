module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  configureWebpack: {
    externals: {
      config: JSON.stringify({
        apiURL: 'http://localhost:5000/api'
      })
    }
  }
}