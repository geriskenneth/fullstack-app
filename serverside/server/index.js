const express = require('express')
const session = require('express-session')
const bodyParser = require('body-parser')
const app = express()
const connectDB = require('./config/db')
const PORT = process.env.PORT || 5000
const cors = require('cors')
const passport = require('passport')
const MongoStore = require('connect-mongo')(session);
const mongoose =  require('mongoose')
// Passport Config
require('./config/passport')(passport);

// Connect database
connectDB()
// tutorial: https://medium.com/@evangow/server-authentication-basics-express-sessions-passport-and-curl-359b7456003d

//Init Middleware
app.use(bodyParser.json({extended: false}))
app.use(bodyParser.urlencoded({extended: false}))
app.use(cors())

// express session middleware setup
const db = mongoose.connection
// app.use(session({
//     secret: 'W$q4=25*8%v-}UV',
//     resave: false,
//     saveUninitialized: true,
//     store: new MongoStore({ mongooseConnection: db}),
//     cookie: {maxAge: 31556952000}
// }));

// passport middleware setup ( it is mandatory to put it after session middleware setup)
app.use(passport.initialize())
app.use(passport.session())

// API routes
app.use('/api/auth', require('./routes/api/auth'))
app.use('/api/profile', require('./routes/api/profile'))
app.use('/api/lectors', require('./routes/api/lectors'))
app.use('/api/categories', require('./routes/api/categories'))
app.use('/api/tags', require('./routes/api/tags'))
app.use('/api/courses', require('./routes/api/courses'))
app.use('/api/subscribers', require('./routes/api/subscribers'))

app.listen(PORT, () => console.log(`server started on port ${PORT}`))