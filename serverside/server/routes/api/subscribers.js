const express = require('express')
const SubscribeRoutes = express.Router()
const Subscriber = require('../../models/SubscriberSchema')

// @route    POST api/subscribers/add
// @desc     Add new subscriber
// @access   Public
SubscribeRoutes.post('/add', async (res, req) => {
    const { firstname, lastname, email, active } = req.body

    try {
        let subscription = await Subscriber.findOne({ email })

        if (subscription) {
            return res.status(400).send({ error: 'Subscriber already exists' })
        }

        subscription = new Subsscriber({
            firstname: firstname,
            lastname: lastname,
            email: email,
            active: active,
            date: Date.now()
          })

        await subscription.save()
        res.status(200).send({ msg: 'New Subscriber has been added' })
        
    } catch (err) {
        res.status(500).send('Server Error')
    }
})

// @route    GET api/subscribers/
// @desc     Get all subscribers
// @access   Public
SubscribeRoutes.get('/', async (req, res)=> {
    try {
        const subscribers = await Subscriber.find().sort({ date: -1 })
        res.send(subscribers)
    } catch (err) {
        res.status(500).send('Server Error')
    }
})

// @route    GET api/subscribers/:subsciber_id
// @desc     Get subscriber details
// @access   Public
SubscribeRoutes.get('/details/:subscriber_id', async (req, res)=> {
    try {
        const subscriber = await Subscriber.findById(req.params.subscriber_id)
    
        if (!subscriber) return res.status(400).send({ error: 'Subscriber not found' })
    
        res.json(subscriber)
    } catch (err) {
        if (err.kind === 'ObjectId') {
          return res.status(400).send({ error: 'Subscriber not found' })
        }
        res.status(500).send('Server Error')
    }
})

module.exports = SubscribeRoutes