const express = require('express')
const profileRoutes = express.Router()

const Profile = require('../../models/ProfileSchema')

//Defined get data(index or listing) route
profileRoutes.get('/me', async (req, res) =>{
    
    try {
        
        // console.log(req.user._id);
        const profile = await Profile.findOne({ user: req.user._id})

        console.log(user);
        if (!profile) 
          return res.status(400).json({ msg: 'There is no profile for this user' })
        
        res.json(profile)
    } catch (err) {
        
        res.status(500).send(req.user)
    }
})

profileRoutes.get('/', (req, res) =>{
    res.send('profile page overview')
})

module.exports = profileRoutes