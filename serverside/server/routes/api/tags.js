const express = require('express')
const TagRoutes = express.Router()
const Tag = require('../../models/TagSchema')

// @route    POST api/tags/add
// @desc     Add new tag
// @access   Public
TagRoutes.post('/add', async (req, res) => {
    const { name, slug, description } = req.body
    
    try {
        let tag = await Tag.findOne({ slug })
       
        if(tag) {
            return res.status(400).send({ error: 'Tag already exists' })
        }

        tag = new Tag({
            name: name,
            slug: slug,
            description: description,
            date: Date.now()
        })

        await tag.save()
        res.status(200).send({ msg: 'New Tag has been added' })
 
    } catch (error) {
        res.status(500).send(req)
    }
})

// @route    GET api/tags/
// @desc     Get all tags
// @access   Public
TagRoutes.get('/', async (req, res) => {
    try {
        const tags = await Tag.find().sort({ date: -1 })
        res.send(tags)
    } catch (error) {
        res.status(500).send(req)
        
    }
})

module.exports = TagRoutes