const express = require('express')
const authRouter = express.Router()
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')
const { check } = require('express-validator')

const auth = require('../../middleware/auth')
const User = require('../../models/UserSchema')

// @route    GET api/auth
// @desc     Test route
// @access   Public
authRouter.get('/', auth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select('-password')

    res.json(user)

  } catch (err) {
    res.status(500).send(err.message)
  }
})

// @route    POST api/auth/login
// @desc     Authenticate user & get token
// @access   Public
authRouter.post(
  '/login',
  [
    check('email', 'Please include a valid email').isEmail(),
    check('password', 'Password is required').exists()
  ],
  async (req, res) => {
    const { email, password } = req.body

    try {
      const user = await User.findOne({ email })
      if (!user) {
        return res.status(400).json({ msg: 'User does not exist' })
      }

      const isMatch = await bcrypt.compare(password, user.password)
      if (!isMatch) {
        return res.status(400).json({ msg: 'Invalid Credentials' })
      }

      const payload = {
        user: {
          id: user.id
        }
      }

      const isLogged = req.cookies['token']

      if(isLogged)
        return res.status(400).json({ msg: 'User is already logged in' })
      else{ 
        jwt.sign(
          payload,
          config.get('jwtToken'),
          { expiresIn: config.get('expiry_date') },
          (err, token) => {
            if (err) throw err

            
            res.cookie('token', token, { maxAge: 31536000000, httpOnly: true })
            return res.send({ msg: 'Successfully logged in', user: user.token })
          }
        )
      }
    } catch (err) {
      res.status(500).send(err.message)
    }
  }
)

// @route    POST auth/register
// @desc     Register auth & encrypt password
// @access   Public
authRouter.post('/register', 
  [
    check('name', 'Name is required')
      .not()
      .isEmpty(),
    check('email', 'Please include a valid email').isEmail(),
    check(
      'password',
      'Please enter a password with 6 or more characters'
    ).isLength({ min: 6 })
  ],
  async (req, res) => {
    const { name, email, password } = req.body

    try {
      //Check if email already exists
      let user = await User.findOne({ email })
      if (user) {
        return res.status(400).json({msg: 'User already exists'})
      }

      //Create new user
      user = new User({
        name,
        email,
        password
      })

      //Encrypt password
      const salt = await bcrypt.genSalt(10)
      user.password = await bcrypt.hash(password, salt)

      await user.save()

      const payload = {
        user: {
          id: user.id
        }
      }

      jwt.sign(
        payload,
        config.get('jwtToken'),
        { expiresIn: config.get('expiry_date') },
        (err, token) => {
          if (err) throw err
          res.json({ token })
        }
      )

      req.login()
      // res.cookie('token', token, { maxAge: config.get('expiry_date'), httpOnly: true })


    } catch (err) {
      res.status(500).send(err.message)
    }
  }
)

authRouter.post('/logout', (req, res) => {
  try {
    // res.clearCookie('token')
    return res.json({ msg: 'User session has ended' })

  } catch (err) {
    res.status(500).send(err)
  }
})
module.exports = authRouter