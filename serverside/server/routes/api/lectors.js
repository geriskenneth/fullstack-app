const express = require('express')
const lectorRoutes = express.Router()
const Lector = require('../../models/LectorSchema')

// @route    POST api/lectors/add
// @desc     Add new lector
// @access   Public
lectorRoutes.post('/add', async (req, res) => {
    const { firstname, lastname, email, title, avatar, active, bio } = req.body

    try {
        let lector = await Lector.findOne({ email })

        if (lector) {
          return res.status(400).send({ error: 'Lector already exists' })
        }

        lector = new Lector({
            firstname: firstname,
            lastname: lastname,
            email: email,
            title: title,
            avatar: avatar,
            bio: bio,
            active: active,
            date: Date.now()
          })

        await lector.save()
        res.status(200).send({ msg: 'New Lector has been added' })

    } catch (err) {
        res.status(500).send(req.user)
    }
})

// @route    GET api/lectors/
// @desc     Get all lectors
// @access   Public
lectorRoutes.get('/', async (req, res) =>{
    try {
        const lectors = await Lector.find().sort({ date: -1 })
        res.send(lectors)
    } catch (err) {
        res.status(500).send('Server Error')
    }
})


// @route    GET api/lectors/:lector_id
// @desc     Get lector details
// @access   Public
lectorRoutes.get('/edit/:lector_id', async (req, res) =>{
    try {
        const lector = await Lector.findById(req.params.lector_id)
    
        if (!lector) return res.status(400).send({ error: 'Lector not found' })
    
        res.json(lector)
      } catch (err) {

        if (err.kind === 'ObjectId') {
          return res.status(400).send({ error: 'Profile not found' })
        }
        res.status(500).send('Server Error')
      }
})

// @route    SET api/lectors/:lector_id
// @desc     Update lector details
// @access   Public
lectorRoutes.post('/update/:lector_id', async (req, res) =>{
  const { firstname, lastname, title, avatar, active, bio } = req.body
  const lector = await Lector.findById(req.params.lector_id)

  console.log(lector);

  try {
      if (lector) {
        lector = await Lector.findOneAndUpdate(
          { lector: params.lector_id },
          { $inc: {            
              firstname: firstname,
              lastname: lastname,
              email: email,
              title: title,
              avatar: avatar,
              bio: bio,
              active: active
            } 
          }
        )

        console.log(lector);
        return res.json(lector)
      }
    } catch (err) {

      if (err.kind === 'ObjectId') {
        return res.status(400).send({ error: 'Profile not found' })
      }
      res.status(500).send('Server Error')
    }
})
module.exports = lectorRoutes