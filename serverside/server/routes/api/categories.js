const express = require('express')
const CategoryRoutes = express.Router()
const Category = require('../../models/CategorySchema')

// @route    POST api/categorys/add
// @desc     Add new category
// @access   Public
CategoryRoutes.post('/add', async (req, res) => {
    const { name, slug, description } = req.body
    
    try {
        let category = await Category.findOne({ slug })
       
        if(category) {
            return res.status(400).send({ error: 'Category already exists' })
        }

        category = new Category({
            name: name,
            slug: slug,
            description: description,
            date: Date.now()
        })

        await category.save()
        res.status(200).send({ msg: 'New Category has been added' })
 
    } catch (error) {
        res.status(500).send(req)
    }
})

// @route    GET api/categorys/
// @desc     Get all categorys
// @access   Public
CategoryRoutes.get('/', async (req, res) => {
    try {
        const categories = await Category.find().sort({ date: -1 })
        res.send(categories)
    } catch (error) {
        res.status(500).send(req)
        
    }
})

module.exports = CategoryRoutes