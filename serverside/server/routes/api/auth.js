const express = require('express')
const authRouter = express.Router()
const { ensureAuthenticated } = require('../../config/auth')
const User = require('../../models/UserSchema')
const passport = require('passport')

// @route    GET api/auth
// @desc     Test route to ensure authentication
// @access   Public
authRouter.get('/', ensureAuthenticated, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select('-password')
    res.send(user)

  } catch (err) {
    res.status(500).send(err.message)
  }
})

// @route    POST api/auth/login
// @desc     Authenticate user & get token
// @access   Public
authRouter.post('/login', (req, res, next) => {
  passport.authenticate("local", (err, user, info) => {
    if (err) {
      return next(err);
    }

    if (!user) {
      return res.status(400).send([user, "Cannot log in", info]);
    }

    req.login(user, err => {
      res.send(user._id)
      // res.send("Logged in");
    })
    
  })(req, res, next);
})

authRouter.post('/logout', (req, res) => {
  req.logout()
  return res.send()
})
module.exports = authRouter