const express = require('express')
const CourseRoutes = express.Router()
const Course = require('../../models/CourseSchema')
 
// @route    POST api/course/add
// @desc     Add new course
// @access   Public
CourseRoutes.post('/add', async (res, req) => {
    const { title, description, difficulty, active } = req.body

    try {
        let course = await Course.findOne({ title })

        if (course) {
            return res.status(400).send({ error: 'Course already exists' })
        }

        course = new Course({
            lector: lector,
            title: title,
            description: description,
            category: category,
            tags: tags,
            difficulty: difficulty,
            thumbnail, thumbnail,
            price: price,
            language: language,
            closed_captions: closed_captions,
            requirements: requirements,
            what_you_learn: what_you_learn,
            active: active,
            date: Date.now()
          })

        await course.save()
        res.status(200).send({ msg: 'New Course has been added' })
        
    } catch (err) {
        res.status(500).send('Server Error')
    }
})

// @route    GET api/course/
// @desc     Get all courses
// @access   Public
CourseRoutes.get('/', async (req, res)=> {
    try {
        const courses = await Course.find().sort({ date: -1 })
        res.send(courses)
    } catch (err) {
        res.status(500).send('Server Error')
    }
})

// @route    GET api/course/:course_id
// @desc     Get course details
// @access   Public
CourseRoutes.get('/edit/:course_id', async (req, res)=> {
    try {
        const course = await Course.findById(req.params.course_id)
    
        if (!course) return res.status(400).send({ error: 'Course not found' })
    
        res.json(course)
    } catch (err) {
        if (err.kind === 'ObjectId') {
          return res.status(400).send({ error: 'Course not found' })
        }
        res.status(500).send('Server Error')
    }
})

// @route    DELETE api/course/:course_id
// @desc     Delete course
// @access   Public

CourseRoutes.get('/delete/:course_id', async (req, res) => {
    try {
        let course = await Course.findById(req.params.course._id)

        if(!course) return res.status(400).send({ msg: 'Course not found' })

       
    } catch (err) {
        res.status(500).send('Server Error')
    }
})
module.exports = CourseRoutes