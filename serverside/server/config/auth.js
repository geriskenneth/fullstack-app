module.exports = {
    ensureAuthenticated: (req, res, next) => {
      if (!req.isAuthenticated()) {
        res.json({ msg: 'not authenticated' })
      }
      else{
        return next()
      } 
    }
  }