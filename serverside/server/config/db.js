const mongoose = require ('mongoose')
const config = require('config')

const connectDB = async () => {
    try{
        await mongoose.connect(config.get('DB'), {
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false
        })
        console.log('MongoDB connected')
    }catch(err){
        console.error(err.message)
        //Exit process when failed connecting
        process.exit(1)
    }
}

module.exports = connectDB