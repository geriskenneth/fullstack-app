const mongoose = require('mongoose')

const LectorSchema = new mongoose.Schema({
  lector: { type: mongoose.Schema.Types.ObjectId, ref: 'Lector' },
  firstname: { type: String },
  lastname: { type: String },
  title: { type: String },
  email: { type: String },
  avatar: {type: String },
  bio: { type: String },
  active: { type: Boolean },
  date: { type: Date, default: Date.now }
}, {collections: 'lectors'})

module.exports = mongoose.model('Lector', LectorSchema)