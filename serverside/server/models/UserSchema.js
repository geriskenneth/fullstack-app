const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    password:{
        type: String,
        required: true
    },
    title:{
        type: String,
        default: 'user'
    },
    address: {
        type: String,
    },
    city: {
        type: String
    },
    phone: {
        type: String
    },
    createdAt: {
        type: Date,
    }
    },{collection: 'users'})

module.exports = mongoose.model('User', UserSchema)