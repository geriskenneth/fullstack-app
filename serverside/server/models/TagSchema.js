const mongoose = require('mongoose')

const TagSchema = new mongoose.Schema({
  tag: { type: mongoose.Schema.Types.ObjectId, ref: 'Tag' },
  name: { type: String },
  slug: { type: String },
  description: { type: String },
  date: { type: Date, default: Date.now }
}, {collections: 'tas'})

module.exports = mongoose.model('Tag', TagSchema)