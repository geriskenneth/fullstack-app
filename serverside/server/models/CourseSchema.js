const mongoose = require('mongoose')

const CourseSchema = new mongoose.Schema({
  lector: { type: mongoose.Schema.Types.ObjectId, ref: 'Lector' },
  title: { type: String },
  description: { type: String },
  category: { type: String },
  tags: { type: Array },
  difficulty: { type: String },
  thumbnail: { type: String },
  price: { type: Number },
  language: { type: String },
  closed_captions: {type: Array },
  requirements: {type: String },
  amount_students: { type: Number },
  what_you_learn: { type: Array },
  active: { type: Boolean },
  date: { type: Date, default: Date.now }
}, {collections: 'courses'})

module.exports = mongoose.model('Course', CourseSchema)