const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
  category: { type: mongoose.Schema.Types.ObjectId, ref: 'Category' },
  name: { type: String },
  slug: { type: String },
  description: { type: String },
  date: { type: Date, default: Date.now }
}, {collections: 'categories'})

module.exports = mongoose.model('Category', CategorySchema)