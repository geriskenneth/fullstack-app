const mongoose = require('mongoose')

const SubscriberSchema = new mongoose.Schema({
  subscriber: { type: mongoose.Schema.Types.ObjectId, ref: 'Subscriber' },
  firstname: { type: String },
  lastname: { type: String },
  email: { type: String },
  avatar: {type: String },
  active: { type: Boolean },
  date: { type: Date, default: Date.now }
}, {collections: 'subscribers'})

module.exports = mongoose.model('Subscriber', SubscriberSchema)